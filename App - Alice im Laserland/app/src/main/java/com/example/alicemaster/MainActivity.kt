package com.example.alicemaster

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager

import android.view.animation.AccelerateDecelerateInterpolator
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.start
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.R.attr.start
import android.R.attr.button
import android.content.Intent
import android.graphics.Color


//import android.R


class MainActivity : AppCompatActivity(){
    private lateinit var bluetoothLeService : BluetoothLeService
    private lateinit var bluetoothAdapter : BluetoothAdapter

    private val HM10_ADDRESS ="18:62:E4:3F:C4:BC"
    //Der Count ist dazu da den Delay des Arduinos zu reduzieren.
    private var count =0
    private var lvlC = 0
    //Level Variablen
    private var level1 = true
    private var level2 = false
    private var level3 = false
    //Richtung der drehenden Spiegel
    private var clockwise = true
    lateinit var buttonChangeDirection: ImageButton
    private var lose = false
    private var restart = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Fullscreen Mode
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        //Image-Buttons und TextViews
        val timeLeftTextView = findViewById<TextView>(R.id.countdown)
        val levelText = findViewById<TextView>(R.id.levelText)
        val button = findViewById<ImageButton>(R.id.button)
        val button2 = findViewById<ImageButton>(R.id.button2)
        val button3 = findViewById<ImageButton>(R.id.button3)
        val button4 = findViewById<ImageButton>(R.id.button4)
        val button5 = findViewById<ImageButton>(R.id.button5)
        val exit = findViewById<ImageButton>(R.id.exit)

        //Zum wechseln der Activity
        val intent = Intent(this, LoseActivity::class.java)
        exit.setOnClickListener {
            val intent = Intent(this, LoseActivity::class.java)
            // start your next activity
            restart = true
            startActivity(intent)

        }


        //Ruft den OnTouchListener bzw. RepeatListener auf, damit man die Button gedrückt halten kann.
        button.setOnTouchListener(RepeatListener(100, 100, object : View.OnClickListener {
            override fun onClick(view: View) {
                count++


                    if (clockwise == true) {
                        if (count % 3 == 0) {
                            bluetoothLeService.write("1")
                        }
                        //lässt die Buttons durch eine Animation in ausgewählter Richtung rotieren
                        val deg = button.getRotation() - 30f;
                        button.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                    }
                 else {
                        if (count % 3 == 0) {
                            //Sendet eine Nachricht über bluetoothLeService an das Bluetooth Modul
                    bluetoothLeService.write("2")}
                    val deg = button.getRotation() + 30f;
                    button.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()

                }

            }
        }))
        //Das Gleiche wie button1
        button2.setOnTouchListener(RepeatListener(100, 100, object : View.OnClickListener {
            override fun onClick(view: View) {
                count++
                if(clockwise==true){
                    if (count % 3 == 0) {
                    bluetoothLeService.write("3")}
                    val deg = button2.getRotation() - 30f;
                    button2.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
                else{
                    if (count % 3 == 0) {
                    bluetoothLeService.write("4")}
                    val deg = button2.getRotation() + 30f;
                    button2.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
            }
        }))
        //Das Gleiche wie button1
        button3.setOnTouchListener(RepeatListener(100, 100, object : View.OnClickListener {
            override fun onClick(view: View) {
                count++
                if(clockwise==true){
                    if (count % 3 == 0) {
                    bluetoothLeService.write("5")}
                    val deg = button3.getRotation() - 30f;
                    button3.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
                else{
                    if (count % 3 == 0) {
                    bluetoothLeService.write("6")}
                    val deg = button3.getRotation() + 30f;
                    button3.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
            }
        }))
        //Das Gleiche wie button1
        button4.setOnTouchListener(RepeatListener(100, 100, object : View.OnClickListener {
            override fun onClick(view: View) {
                count++
                if(clockwise==true){
                    if (count % 3 == 0) {
                    bluetoothLeService.write("7")}
                    val deg = button4.getRotation() - 30f;
                    button4.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
                else{
                    if (count % 3 == 0) {
                    bluetoothLeService.write("8")}
                    val deg = button4.getRotation() + 30f;
                    button4.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
            }
        }))
        //Das Gleiche wie button1
        button5.setOnTouchListener(RepeatListener(100, 100, object : View.OnClickListener {
            override fun onClick(view: View) {
                count++
                if(clockwise==true){
                    if (count % 3 == 0) {
                    bluetoothLeService.write("9")}
                    val deg = button5.getRotation() - 30f;
                    button5.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
                else{
                    if (count % 3 == 0) {
                    bluetoothLeService.write("0")}
                    val deg = button5.getRotation() + 30f;
                    button5.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()
                }
            }
        }))

        //Setzt die Bluetooth Connection mit vorgegebener MAC Adresse und verbindet sich automatisch mit dieser
        setupBluetoothConnection()

        //Timer der abläuft... bei 0 wechselt die Activity zum Restartscreen
        val timer = Timer("zeit", true)

        var time = 600
        timer.scheduleAtFixedRate(1000,1000){
            println("ok")
            if(time >0){
                time--
                //Spielt Sound ab
                val tickFX = MediaPlayer.create(this@MainActivity, R.raw.tick_clock)
                tickFX.start()
                //Ist notwenig, ansonsten werden die sounds nach 15 maligen abspielen nicht mehr abgespielt.
                tickFX.setOnCompletionListener(OnCompletionListener { mp -> mp.release() })

            }
            if (restart ==true){
                time =0
                restart = false
            }
            var handler = Handler(Looper.getMainLooper())
            handler.post{
                //Zeigt die übrigen Sekunden an. (Bewusst keine Minuten als anzeige Variante gewählt, da Es den Spieler etwas verwirren soll bzw. läuft in der Welt von Alice alles ein wenig anders)
                timeLeftTextView.setText(time.toString())
            }
            if(time ==0 && lose == false){
                //Spiel ist verloren
                lose = true
                val loseFX = MediaPlayer.create(this@MainActivity, R.raw.loose_sound)
                loseFX.start()
                loseFX.setOnCompletionListener(OnCompletionListener { mp -> mp.release() })

                // start your next activity
                startActivity(intent)


            }
        }

    }


    //Initialisiert die Bluetoothverbindung
    private fun setupBluetoothConnection(){
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
        bluetoothLeService = BluetoothLeService(bluetoothManager)
        bluetoothLeService.initialize()
        //Mac Adresse
        bluetoothLeService.connect(HM10_ADDRESS)
    }
    //Wechselt das Level
    fun changeLevel(view: View){

        val levelFX = MediaPlayer.create(this@MainActivity, R.raw.levelchange)
        levelFX.start()
        var temp = true
        if(level3== true){
            lvlC++
            //ändert die Leuchte im Kasten zur anzeige des zu treffenden Ziels
            bluetoothLeService.write("c")
            level1=true
            level2=false
            level3=false
            temp = false
            levelText.setText("1")

        }
        if(level2 == true){
            lvlC++
            //ändert die Leuchte im Kasten zur anzeige des zu treffenden Ziels
            bluetoothLeService.write("b")
            level3=true
            level1=false
            level2= false
            levelText.setText("3")


        }

        if (level1 == true && temp ==true){
            lvlC++
            //ändert die Leuchte im Kasten zur anzeige des zu treffenden Ziels
            bluetoothLeService.write("a")
            level2 = true
            level1 = false
            level3 = false
            levelText.setText("2")

        }


    }
    //Der Button zum Richtungs wechsel wird gedrückt
    fun changeDirection(view: View){
        val directionFX = MediaPlayer.create(this@MainActivity, R.raw.change_direction_sound)
        directionFX.start()
        directionFX.setOnCompletionListener(OnCompletionListener { mp -> mp.release() })


        buttonChangeDirection = findViewById<ImageButton>(R.id.changeDirection)
        if(clockwise == true){
            val deg = buttonChangeDirection.getRotation() + 180f;
            buttonChangeDirection.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()

            clockwise = false
        }
        else{
            val deg = buttonChangeDirection.getRotation() - 180f;
            buttonChangeDirection.animate().rotation(deg).interpolator = AccelerateDecelerateInterpolator()

            clockwise = true;
        }
    }
    //Sollte nach dem gewinnen oder verlieren die Spiegel durcheinander bringen,
    //durch den anfangs starken Delay hat sich diese aber als zu anspruchsvoll für die Motoren herrausgestellt.
    private fun resetMirrors(){
        for (i in 20 downTo 0 step 1) {

            bluetoothLeService.write("1")
            //bluetoothLeService.write("2")
            bluetoothLeService.write("3")
            //bluetoothLeService.write("4")
            bluetoothLeService.write("5")
            //bluetoothLeService.write("6")
            bluetoothLeService.write("7")
            //bluetoothLeService.write("8")
            bluetoothLeService.write("9")
            //bluetoothLeService.write("0")*/

        }
    }


}